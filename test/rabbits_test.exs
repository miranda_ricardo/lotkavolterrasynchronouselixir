defmodule RabbitsTest do
  use ExUnit.Case


  setup_all do
    rabbits = %{population: 1, birth_rate: 1, death_rate: 1}

    {:ok, rab_stash_pid} = LotkaVolterra.RabbitsStash.start_link rabbits
    LotkaVolterra.Rabbits.start_link rab_stash_pid
    {:ok, [rab_stash_pid: rab_stash_pid, rabbits: rabbits]}
  end


  test "Compute rabbits population 1" do
    assert LotkaVolterra.Rabbits.new_rabbits(1, 1, 1, 1, 1) == 1
  end


  test "Compute rabbits population 2" do
    assert LotkaVolterra.Rabbits.new_rabbits(1, 1, 1, 1, 0.5) == 1
  end


  test "Compute rabbits population 3" do
    assert LotkaVolterra.Rabbits.new_rabbits(1, 1, 1.1, 1, 1) == 1.1
  end
end
