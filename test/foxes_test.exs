defmodule FoxesTest do
  use ExUnit.Case


  setup_all do
    foxes = %{population: 1, birth_rate: 1, death_rate: 1}

    {:ok, fox_stash_pid} = LotkaVolterra.FoxesStash.start_link foxes
    LotkaVolterra.Foxes.start_link fox_stash_pid
    {:ok, [fox_stash_pid: fox_stash_pid, foxes: foxes]}
  end


  test "Compute foxes population 1" do
    assert LotkaVolterra.Foxes.new_foxes(1, 1, 1, 1, 1) == 1
  end


  test "Compute foxes population 2" do
    assert LotkaVolterra.Foxes.new_foxes(1, 1, 1, 1, 0.5) == 1
  end


  test "Compute foxes population 3" do
    assert LotkaVolterra.Foxes.new_foxes(1, 1, 1.1, 1, 1) == 1.1
  end
end
