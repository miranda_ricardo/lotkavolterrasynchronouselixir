defmodule LotkaVolterraTest do
  use ExUnit.Case


  setup_all do
    time = %{time_remaining: 5, delta_t: 0.001, time_elapsed: 0}

    {:ok, lv_stash_pid} = LotkaVolterra.LotkaVolterraStash.start_link time
    LotkaVolterra.LotkaVolterra.start_link lv_stash_pid
    {:ok, [lv_stash_pid: lv_stash_pid, time: time]}
  end


  test "LV stash initial values", %{lv_stash_pid: lv_stash_pid, time: time} do
    assert LotkaVolterra.LotkaVolterraStash.get(lv_stash_pid) == time
  end


  test "LV initial values", %{lv_stash_pid: _, time: time} do
    {lotka_volterra, _} = LotkaVolterra.LotkaVolterra.get  
    assert lotka_volterra ==  time
  end
end
