defmodule LotkaVolterra do
  @moduledoc """
  Copyright 2015 (c) Ricardo C. Miranda 

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use Application


  def start(_type, _args) do
    IO.puts "Starting Lotka Volterra application."
    LotkaVolterra.Supervisor.start_link 
  end
end
