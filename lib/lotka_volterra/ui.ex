defmodule LotkaVolterra.UI do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  #####
  ## Client API


  @doc """
  Starts the UI. 
  """
  def start_link do
    :gen_server.start_link({ :local, :ui_pid }, __MODULE__, [], []) 
  end


  @doc """
  Options to run the model.
  """
  def run_model(run_type) do
    :gen_server.cast :ui_pid, {:run_model, run_type}
  end
  

  #####
  ## Server Callbacks


  def init(_) do
    { :ok, :ui }
  end


  def handle_cast({:run_model, :step_by_step}, _) do
    {rabbits, foxes, time} = LotkaVolterra.LotkaVolterra.calc_step 
    IO.puts "#{time}, #{rabbits}, #{foxes}"

    { :noreply, :ui }
  end


  def handle_cast({:run_model, :once}, _) do
    time_remainig = LotkaVolterra.LotkaVolterra.get :time
    run_once time_remainig, 0
    { :noreply, :ui }
  end


  #####
  # UI


  defp run_once(time_remaining, acc) when time_remaining >= 0.0 do
    {rabbits, foxes, time} = LotkaVolterra.LotkaVolterra.calc_step 
    res = %{key: "lv:#{acc}", value: %{time: time, rabbits: rabbits, foxes: foxes}}
    IO.puts "#{res.value.time}, #{res.value.rabbits}, #{res.value.foxes}"
    Redis.RedisOutput.set res
    run_once(LotkaVolterra.LotkaVolterra.get(:time), acc + 1)
  end


  defp run_once(_, _) do
    IO.puts "End of simulation."
  end
end
