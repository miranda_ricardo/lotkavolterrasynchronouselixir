defmodule LotkaVolterra.LotkaVolterra do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  ## Client API


  @doc """
  Starts the application, serves as the scheduler. 
  """
  def start_link(l_stash_pid) do
    :gen_server.start_link({ :local, :lv_pid }, __MODULE__, l_stash_pid, []) 
  end


  @doc """
  Computes model next time step.
  """
  def calc_step do
     :gen_server.call :lv_pid, :calc_step
  end
  

  @doc """
  :time -> get time reamining to the end of computation.
  -     -> get state.
  """
  def get(opt \\ :state) do
    :gen_server.call :lv_pid, {:get, opt}
  end


  ## Server Callbacks


  def init(l_stash_pid) do
    time = LotkaVolterra.LotkaVolterraStash.get l_stash_pid
    { :ok, {time, l_stash_pid} }
  end


  def handle_call(:calc_step, _from, {time, l_stash_pid}) do
    %{population: foxes}   = LotkaVolterra.Foxes.get
    %{population: rabbits} = LotkaVolterra.Rabbits.get

    %{population: new_foxes}   = LotkaVolterra.Foxes.foxes rabbits, time.delta_t 
    %{population: new_rabbits} = LotkaVolterra.Rabbits.rabbits foxes, time.delta_t 

    time_elapsed   = time.time_elapsed   + time.delta_t
    time_remaining = time.time_remaining - time.delta_t

    { :reply, 
      {new_rabbits, new_foxes, time_elapsed}, 
      {%{time_remaining: time_remaining, time_elapsed: time_elapsed, delta_t: time.delta_t}, l_stash_pid} } 
  end


  def handle_call({:get, opt}, _from, {time, l_stash_pid}) do
    case opt do 
      :time ->
        { :reply, time.time_remaining, {time, l_stash_pid} }

      _     ->
        { :reply, {time, l_stash_pid}, {time, l_stash_pid} }
  end
  end


  def terminate(_reason, {time, l_stash_pid}) do
    LotkaVolterra.LotkaVolterraStash.set l_stash_pid, time
  end
end
