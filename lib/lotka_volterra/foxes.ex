defmodule LotkaVolterra.Foxes do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  ## Client API


  @doc """
  Initial foxes population.
  """
  def start_link(f_stash_pid) do
    :gen_server.start_link({ :local, :fox_pid }, __MODULE__, f_stash_pid, [])
  end


  @doc """
  Get current foxes population.
  """
  def get do
    :gen_server.call :fox_pid, :get
  end


  @doc """
  Synchronous computation of next time step foxes population.
  """
  def foxes(rabbits, delta_t) do
    :gen_server.call :fox_pid, {:foxes, rabbits, delta_t}
  end


  ## Server Callbacks


  def init(f_stash_pid) do
    foxes = LotkaVolterra.FoxesStash.get f_stash_pid
    { :ok, {foxes, f_stash_pid} }
  end


  def handle_call(:get, _from, {foxes, f_stash_pid}) do
    { :reply, foxes, {foxes, f_stash_pid} }
  end


  def handle_call({:foxes, rabbits, delta_t}, _from, {foxes, f_stash_pid})
    when is_number(rabbits)
    and  is_number(delta_t) do

    n_foxes = put_in(foxes.population, new_foxes(rabbits,
                                                 foxes.population,
                                                 foxes.birth_rate,
                                                 foxes.death_rate,
                                                 delta_t))
    { :reply, n_foxes, {n_foxes, f_stash_pid} }
  end


  def terminate(_reason, {foxes, f_stash_pid}) do
    LotkaVolterra.FoxesStash.set f_stash_pid, foxes
  end


  ## Algorithms


  def new_foxes(r, f, br, dr, dt) when is_number(r)
                                  and  is_number(f)
                                  and  is_number(br)
                                  and  is_number(dr) 
                                  and  is_number(dt) do
    f + (br * r * f - dr * f) * dt
  end
end

