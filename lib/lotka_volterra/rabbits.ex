defmodule LotkaVolterra.Rabbits do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  ## Client API


  @doc """
  Initial rabbits population.
  """
  def start_link(r_stash_pid) do
    :gen_server.start_link({ :local, :rab_pid }, __MODULE__, r_stash_pid, [])
  end


  @doc """
  Get current rabbits population.
  """
  def get do
    :gen_server.call :rab_pid, :get
  end


  @doc """
  Synchronous computation of next time step rabbits population.
  """
  def rabbits(foxes, delta_t) do
    :gen_server.call :rab_pid, {:rabbits, foxes, delta_t}
  end


  ## Server Callbacks


  def init(r_stash_pid) do
    rabbits = LotkaVolterra.RabbitsStash.get r_stash_pid
    { :ok, {rabbits, r_stash_pid} }
  end


  def handle_call(:get, _from, {rabbits, r_stash_pid}) do
    { :reply, rabbits, {rabbits, r_stash_pid} }
  end


  def handle_call({:rabbits, foxes, delta_t}, _from, {rabbits, r_stash_pid})
    when is_number(foxes)
    and  is_number(delta_t) do

    n_rabbits = put_in(rabbits.population, new_rabbits(rabbits.population, 
                                                       foxes, 
                                                       rabbits.birth_rate, 
                                                       rabbits.death_rate, 
                                                       delta_t))
    { :reply, n_rabbits, {n_rabbits, r_stash_pid} }
  end


  def terminate(_reason, {rabbits, r_stash_pid}) do
    LotkaVolterra.RabbitsStash.set r_stash_pid, rabbits 
  end


  ## Algorithms


  def new_rabbits(r, f, br, dr, dt) when is_number(r)
                                    and  is_number(f)
                                    and  is_number(br)
                                    and  is_number(dr) 
                                    and  is_number(dt) do
    r + (br * r - dr * r * f) * dt
  end
end

