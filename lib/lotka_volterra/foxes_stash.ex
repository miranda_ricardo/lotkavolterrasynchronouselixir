defmodule LotkaVolterra.FoxesStash do
  @moduledoc """
  Copyright (c) 2015 Ricardo C. Miranda

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
  """


  use GenServer


  #####
  ## Client API


  @doc """
  Saves initial state for foxes. 
  """
  def start_link(foxes = %{population: _, birth_rate: _, death_rate: _}) do
    :gen_server.start_link(__MODULE__, foxes, []) 
  end


  def get(pid) do
     :gen_server.call pid, :get
  end
  

  def set(pid, foxes = %{population: _, birth_rate: _, death_rate: _}) do
    :gen_server.cast pid, {:set, foxes}
  end


  #####
  ## Server Callbacks


  def init(foxes) do
    { :ok, foxes }
  end


  def handle_cast({:set, foxes}, _) do
    { :noreply, foxes }
  end


  def handle_call(:get, _from, foxes) do
    { :reply, foxes, foxes }
  end
end
