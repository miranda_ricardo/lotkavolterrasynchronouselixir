Lotka_volterra
=============
This program implements Lotka Volterra ecological model. There is a prey population (rabbits) and a predator population (foxes). We assume:

- rabbits population, R, is controlled by the pressure of hunting with an infinite supply of food;

- foxes, F, source of food are the rabbits so.<\ul>

The set of first order differential equations is:

- \frac{\partial R}{\partial t}= \alpha R -\beta R F, (rabbits);

- \frac{\partial F}{\partial t}= \delta R F -\gamma F, (foxes).

alpha, beta, delta and gamma are positive parameters that represent:

- alpha, rabbits birth rate;

- beta, preying pressure on rabbits;

- delta, foxes birth rate; and

- gamma, foxes natural death and migration due to food pressure.
